# Meals API

This is my coding assessment for EcoCart. I'm excited to be talking with you all.

I decided against working in a larger framework, opting instead to build this with Express and any other packages I could rely on. To me, this better captures the spirit of the exercise, and better simulates what might be done for small services (as opposed to large / monolithic servers). 

### Noteable libraries and notes
- [Express.js](https://expressjs.com/)
- [Zod](https://zod.dev/) - This was a great excuse to research and try some data input validation libraries in the Node ecosystem. I landed on Zod as something lightweight, with an easy to understand API surface that's very simple to get started with.
- [Jest](https://jestjs.io/) - I did not attempt to thoroughly unit test. Rather, I wrote a few tests for the logic layer, just to test that my data validations were working. The only other way to verify that these work the way I want would have been to implement the rest of the app, and then run through scenarios manually via Postman.

## Setup
You must have Node `>=18.0.0` installed to run. This app makes use of [Node-native Fetch](https://dev.to/andrewbaisden/the-nodejs-18-fetch-api-72m), which is considered unstable in version `17.x`, and absent in earlier versions.

Copy the contents of `.env.shadow` to `.env`, and swap in any values if need be. You probably won't need to.

## Running and testing 
To start the dev server:
```
yarn
yarn start:dev
```

To build the production bundle
```
yarn
yarn build
```

To build for production and run locally
```
yarn
yarn start
```

Once the server is running, you can manually test functionality by opening postman, and making a GET request to `http://localhost:8080/api/meals/ingredient/broccoli`. Sub in `broccoli` with whatever ingredient you want. Make sure you URL encode it, e.g. `chicken%20breast`. `chicken_breast` will also work, but URL encoding would be necessary for any non-standard characters.