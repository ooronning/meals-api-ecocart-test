import app from './src/app';
import config from './src/config';

const server = app.listen(config.PORT, () => {
  console.log(`Now listening on port ${config.PORT}...`)
});

