import express from 'express';

const router = express.Router();

router.route('/').get((req, res) => {
  res.json({ body: 'Hello, world!' });
});

router.route('/error').get((req, res, next) => {
  next(new Error());
})

export default router;