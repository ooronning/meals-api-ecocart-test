import express from 'express';
import { getMealsByMainIngredient } from '../logic/meal.logic';

const router = express.Router();

router.route('/ingredient/:ingredient').get(async (req, res) => {
  const result = await getMealsByMainIngredient(req.params.ingredient);
  res.statusCode = result.statusCode;
  res.json(result.body);
})

export default router;