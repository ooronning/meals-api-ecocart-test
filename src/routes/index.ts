import express from 'express';

import helloRoute from './hello.routes';
import mealsRoutes from './meals.routes';

const router = express.Router();

const routes = [
  {
    path: '/hello',
    route: helloRoute
  },
  {
    path: '/meals',
    route: mealsRoutes
  }
];

routes.forEach((route) => {
  router.use(route.path, route.route);
});

export default router;