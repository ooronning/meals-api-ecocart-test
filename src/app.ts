import cors from 'cors';
import express from 'express';
import expressWinston from 'express-winston';
import helmet from 'helmet';
import winston from 'winston';

import config from './config';
import routes from './routes';

const app = express();

app.use(express.json());

app.use(cors());
app.options('*', cors())

if (config.NODE_ENV !== 'test') {
  app.use(expressWinston.logger({
    transports: [
      new winston.transports.Console()
    ],
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.json()
    )
  }));
}

// set security HTTP headers
app.use(helmet());

app.use('/api', routes);

if (config.NODE_ENV !== 'test') {
  app.use(expressWinston.errorLogger({
    transports: [
      new winston.transports.Console()
    ],
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.json()
    )
  }));
}

export default app;