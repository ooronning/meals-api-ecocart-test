import { Meal, MealIngredient } from '../types/meal';
import { LogicError, RouteResponse } from '../types/api'; 
import { mealdbApi } from '../constants';
import { 
  MealByIngredientInput,
  mealByMainIngredientValidator, 
  MealByNameInput, 
  mealByNameValidator
} from '../validations/mealdb.validation';
import { ZodError } from 'zod';

export const getMealsByMainIngredient = async (ingredient: string): Promise<RouteResponse<Meal[]>> => {
  try {
    const mealsByIngredientData = await getMealsByIngredientAPIData(ingredient);
    if (mealsByIngredientData === null) {
      return {
        statusCode: 404,
        body: 'Not found'
      }
    }
    // We can't simply fetch all meals by name in a forEach loop. ForEach won't support multiple async operations.
    // Let's batch up requests with promise.all, so that we call getMealByname for each of the above,
    // storing them in their own place.
    // flatten the results so that it's one list.
    // then for each, find the mealByIngredientData with the same idMeal, and and create new Meal with the union.
    const mealsResponses = await Promise.all(
      mealsByIngredientData.map((meal) => getMealsByNameAPIData(meal.strMeal))
    );
    const mealsByName = mealsResponses.flat();
    const results = mealsByName.map((mealByName) => {
      const mealByIngredientRecord = mealsByIngredientData.find((mealByIngredient) => 
        mealByName.idMeal === mealByIngredient.idMeal
      );

      // This seems to be an error I encountered searching for `ham`.
      // The relevant mealId that can't be found in `mealsByIngredientData` is 52981...
      // and yet, I can find it from the API by querying www.themealdb.com/api/json/v1/1/lookup.php?i=52981
      // This would deserve investigation if this were a real project.
      if (!mealByIngredientRecord) {
        throw new LogicError(`In mealsByName, no matching mealByIngredient for id ${mealByName.idMeal}`)
      }

      const ingredients: MealIngredient[] = [];
      Object.getOwnPropertyNames(mealByName).forEach((propName) => {
        if (propName.includes('strIngredient')) {
          // @ts-ignore -- naturally, the compiler has no way of verifying these properties. 
          // One could accomodate by running some extra validation, or one could find
          // a proper TS solution. For now, I don't care too much.
          const ingredient: string = mealByName[propName];
          if (!ingredient) {
            return;
          }
          const num = propName.slice(13) // get number to match with strMeasure
          // @ts-ignore -- see explanation above
          const measurement: string = mealByName[`strMeasure${num}`]
          ingredients.push({ ingredient, measurement })
        }
      })

      return {
        id: mealByIngredientRecord.idMeal,
        name: mealByIngredientRecord.strMeal,
        instructions: mealByName.strInstructions,
        tags: mealByName.strTags,
        thumbUrl: mealByIngredientRecord.strMealThumb,
        youtubeUrl: mealByName.strYoutube,
        ingredients
      }
    });

    return {
      statusCode: 200,
      body: results
    };
  } catch (error) {
    if (error instanceof ZodError) {
      return {
        statusCode: 500,
        body: error.issues
      }
    }
    if (error instanceof LogicError) {
      return {
        statusCode: 500,
        body: error.message
      }
    }

    return {
      statusCode: 500,
      body: 'Something Went Wrong'
    }
  }
};

export const getMealsByIngredientAPIData = async (ingredient: string): Promise<MealByIngredientInput[] | null> => {
  const res = await fetch(mealdbApi.getMealsByIngredient(ingredient));
  const data = await res.json();
  return mealByMainIngredientValidator.array().nullable().parse(data.meals);
};

export const getMealsByNameAPIData = async (name: string): Promise<MealByNameInput[]> => {
  const res = await fetch(mealdbApi.getMealByName(name));
  const data = await res.json();
  return mealByNameValidator.array().parse(data.meals);
};