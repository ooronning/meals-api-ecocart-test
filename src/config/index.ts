import dotenv from 'dotenv';

dotenv.config();

interface ENV {
  NODE_ENV?: string;
  PORT?: number;
  MEALDB_API_KEY?: string;
}

const config: ENV = {
  NODE_ENV: process.env.NODE_ENV ?? 'dev',
  PORT: process.env.PORT ? Number(process.env.PORT) : undefined,
  MEALDB_API_KEY: process.env.MEALDB_API_KEY,
}

export default config;