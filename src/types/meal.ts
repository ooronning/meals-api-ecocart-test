export interface MealIngredient {
  ingredient: string;
  measurement: string;
}

export interface Meal {
  id: number;
  name: string;
  instructions: string;
  tags?: string[] | null;
  thumbUrl: string;
  youtubeUrl: string;
  ingredients: MealIngredient[]; // each element maps strIgredientN to strMeasureN
}
