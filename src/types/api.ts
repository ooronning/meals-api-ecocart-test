import { ZodIssue } from 'zod';

export interface RouteResponse<T> {
  body: RouteResponseBody<T>;
  statusCode: number;
}

export type RouteResponseBody<T> = T | ZodIssue[] | string | null;

export class LogicError extends Error {}