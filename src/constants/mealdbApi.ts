import config from '../config';

const mealdbBaseURL = 'https://themealdb.com';
const mealdbAPI = `${mealdbBaseURL}/api/json/v1/${config.MEALDB_API_KEY}`;

export const getMealsByIngredient = (ingredient: string) => 
  `${mealdbAPI}/filter.php?i=${encodeURIComponent(ingredient)}`;
export const getMealByName = (name: string) => 
  `${mealdbAPI}/search.php?s=${encodeURIComponent(name)}`;
