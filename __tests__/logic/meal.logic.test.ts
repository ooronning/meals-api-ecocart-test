import { getMealsByIngredientAPIData, getMealsByNameAPIData } from "../../src/logic/meal.logic";
import { jest } from '@jest/globals';
import fetchMock, { enableFetchMocks } from 'jest-fetch-mock';
enableFetchMocks();

const mealsByIngredientMock = {
  meals: [{
    idMeal: '12345',
    strMeal: 'steak tartar',
    strMealThumb: 'thinking_emoji.jpg'
  }]
};

const mealsByIngredientOutputMock = [{
  idMeal: 12345,
  strMeal: 'steak tartar',
  strMealThumb: 'thinking_emoji.jpg'
}];

const mealsByNameMock = {
  meals: [{
    idMeal: '12345',
    strInstructions: 'test one two three',
    strTags: 'meat,cheese',
    strYoutube: 'test',
    strIngredient1: 'meat',
    strMeasure1: '12',
    strIngredient2: 'cheese',
    strMeasure2: '8 lbs',
    strIngredient3: null,
    strMeasure3: null,
  }]
};

const mealsByNameOutputMock = [{
  idMeal: 12345,
  strInstructions: 'test one two three',
  strTags: ['meat', 'cheese'],
  strYoutube: 'test',
  strIngredient1: 'meat',
  strMeasure1: '12',
  strIngredient2: 'cheese',
  strMeasure2: '8 lbs',
  strIngredient3: null,
  strMeasure3: null,
}];

describe('meal API logic', () => {
    describe('getMealsByIngredientAPIData', () => {
      beforeEach(() => {
        jest.clearAllMocks();
      });

      it('calls fetch', async () => {
        fetchMock.mockResponseOnce(JSON.stringify(mealsByIngredientMock))
        await getMealsByIngredientAPIData('steak');
        expect(fetchMock).toHaveBeenCalled();
      });

      it('validates correct response data', async () => {
        fetchMock.mockResponseOnce(JSON.stringify(mealsByIngredientMock))
        const data = await getMealsByIngredientAPIData('steak');
        expect(data).toEqual(mealsByIngredientOutputMock);
      });

      it ('throw for invalid data', async () => {
        fetchMock.mockResponseOnce(JSON.stringify({
          meals: [{
            id: '12345',
            name: 'steak tartar',
            strMealThumb: 'thinking_emoji.jpg'
          }]
        }))
        await expect(getMealsByIngredientAPIData('steak')).rejects.toThrow();
      })
  });

  describe ('getMealsByNameAPIData', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });

    it('calls fetch', async () => {
      fetchMock.mockResponseOnce(JSON.stringify(mealsByNameMock))
      await getMealsByNameAPIData('meat');
      expect(fetchMock).toHaveBeenCalled();
    });

    it('validates correct response data', async () => {
      fetchMock.mockResponseOnce(JSON.stringify(mealsByNameMock))
      const data = await getMealsByNameAPIData('meat');
      expect(data).toEqual(mealsByNameOutputMock);
    });

    it ('throw for invalid data', async () => {
      fetchMock.mockResponseOnce(JSON.stringify({
        meals: [{
          id: '12345',
          Instructions: 'test one two three',
          Tag: 'meat,cheese',
          Youtube: 'test',
          Ingredient1: 'meat',
          Measure1: '12',
          Ingredient2: 'cheese',
          Measure2: '8 lbs',
          Ingredient3: null,
          Measure3: null,
        }]
      }))
      await expect(getMealsByNameAPIData('meat')).rejects.toThrow();
    })
  });
});
